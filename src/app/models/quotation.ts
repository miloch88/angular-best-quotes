import { StringifyOptions } from "querystring";

export interface Quotation {
    author: string;
    sentence: string;
    votes: number;
}