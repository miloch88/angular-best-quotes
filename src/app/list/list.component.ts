import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Quotation } from '../models/quotation';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  @Input()
  quotes: Quotation[];

  @Output()
  vote = new EventEmitter<QuotationEvent>();

  addVote(quatation: Quotation, value: number){
    this.vote.emit({quatation,value})
  }

}

export interface QuotationEvent {
  quatation : Quotation;
  value : number;
}