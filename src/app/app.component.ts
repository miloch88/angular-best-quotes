import { Component } from '@angular/core';
import { QUOTES } from './models/database';
import { Quotation } from './models/quotation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  quotes: Quotation[] = QUOTES;
  titleRanking: string = '';
  titleNav: string = 'Najlepsze cytaty';

  sortByBestVotes(): Quotation[] {
    return [...this.quotes].sort((a, b) => b.votes - a.votes);
  }

  sortByTheWorstVotes(): Quotation[] {
    return [...this.quotes].sort((a, b) => a.votes - b.votes);
  }

  onNewQuotation(quotation: Quotation){
    this.quotes.unshift(quotation);
  }

  addVote(quotation: Quotation, value: number){
    quotation.votes += value;
  }

  bestQuotes(){
    this.titleRanking = 'Najlepsze';
    return this.quotes.filter(q => q.votes > 0).sort((a, b) => b.votes - a.votes);
  }

  worstQuotes(){
    this.titleRanking = 'Najgorsze';
    return this.quotes.filter(q => q.votes < 0).sort((a, b) => a.votes - b.votes);
  }
}
